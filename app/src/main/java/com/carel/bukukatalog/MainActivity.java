package com.carel.bukukatalog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvBuku;
    private List<Buku> list;
    private RecyclerView.ItemDecoration decoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupEnv();
        setupList();

        loadDataDummy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_about, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_profile:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
        }
        return true;
    }

    private void setupEnv() {
        rvBuku = (RecyclerView) findViewById(R.id.rvBuku);
        list = new ArrayList<Buku>();
        decoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
    }

    private void setupList() {
        BukuAdapter adapter = new BukuAdapter(list);
        rvBuku.setLayoutManager(new LinearLayoutManager(this));
        rvBuku.addItemDecoration(decoration);
        rvBuku.setAdapter(adapter);
    }

    private void loadDataDummy() {
        list.add(new Buku(
                R.drawable.sigit_parikesit,
                "Untold Story Jack MA, Pendiri Alibaba.com: No.1 Richest Billionaires In China",
                "10 April 2016",
                "https://s3.bukalapak.com/img/323361993/w-1000/5_f2CwAAQBAJ.jpg",
                "Sigit Parikesit",
                "“Bukan Kerja Keras, Rajin, dan Pintar yang Membuat Kita Kaya Raya. Sukses Itu Karena Nasib Baik.\" ~Miliader JACK MA ~\n" +
                        "Jack Ma adalah salah satu sosok sukses yang telah membuktikan bahwa konsistensi, kerja keras, dan ketekunan dalam belajar bisa membawanya mendirikan perusahaan bisnis berbasis online terbesar di Tiongkok Alibaba grup.\n" +
                        "Buku ini hadir untuk menginspirasi kita semua akan bisnis e-commerce dan filosofi timur dalam berbisnis.Daftar Isi sudah tercover di google play book memudahkan membaca dan mencari cepat."
        ));

        list.add(new Buku(
                R.drawable.muzarini,
                "Grudge My Husband: Eternity Publishing",
                "8 April 2019",
                "https://books.google.com/books/content/images/frontcover/uSaRDwAAQBAJ?fife=w200-h300",
                "Muzarini",
                "Kejadian pembunuhan 2 tahun silam menjadikan Zara korban kesalahpahaman dan terjebak di bawah kungkungan Aldrick, seorang pria yg tampan nan arrogant yang amat sangat membencinya dan memaksa menikahi dirinya. Dan bermulalah kehidupan Zara yang penuh dengan kesakitan dan kesedihan\n" +
                        "\"Ssakitt....\" lirih Zara\n" +
                        "\"Sakit hah! Bagaimana rasanya di tikam? Apa itu tidak sakit? Jawab!!\" bentak Aldrick\n" +
                        "\"Kau memang sudah gila!!\" teriak Zara.\n" +
                        "\"Plakk!\" sekali \"plakk!\" dua kali pria itu menampar kuat pipi Zara, istrinya sendiri.\n" +
                        "\"Jadi inikah tujuanmu menikahiku?\"\n" +
                        "Aldrick tertawa keras bak iblis paling kejam yang begitu mengerikan di mata Zara."
        ));

        list.add(new Buku(
                R.drawable.itsviy,
                "My Bastard Husband: Venom Publisher",
                "15 Mei 2018",
                "https://books.google.com/books/content/images/frontcover/7s5aDwAAQBAJ?fife=w200-h300",
                "Itsviy",
                "Kisah tentang seorang wanita yang baru menginjak usia 20 tahun namun sangat kuat menghadapi masalah demi masalah dalam hidupnya.\n" +
                        "\n" +
                        "Sebatang kara. Itulah dua kata yang dapat mendeskripsikan dirinya secara keseluruhan.\n" +
                        "Dia kehilangan orangtuanya serta sahabat satu-satunya saat masih berusia 15 tahun dan karena kejadian tersebut membuatnya harus belajar mandiri diusianya yang masih terbilang sangat muda. Dan lagi, ia bertahan menjalani hidupnya karena permintaan terakhir sahabatnya.\n" +
                        "Karena beban hidup yang harus ditanggungnya sangat besar dan juga keuangannya yang tidak kunjung membaik. Serta, karena hatinya yang begitu rapuh dan mudah kasihan dengan orang lain. Ia mengambil sebuah keputusan yang sangat salah dalam hidupnya. Yaitu menikah dengan James Alvarado Rodriguez, pengusaha kaya raya.\n" +
                        "Semua masalah bermula semenjak ia menjadi istri James.\n" +
                        "Koreksi!\n" +
                        "Lebih tepatnya ...\n" +
                        "Istri yang tidak diakui oleh James.\n" +
                        "Pria itu selalu bersiap dingin dan kejam dengannya.\n" +
                        "Lalu, ketika semuanya mulai berjalan dengan baik. Tiba-tiba saja, masalah datang lagi dalam kehidupannya seolah tidak kenal bosan untuk menganggu kehidupannya.\n" +
                        "James, pria yang berstatus sebagai suaminya ternyata adalah kekasih sahabatnya. Sebuah fakta yang sungguh mengejutkan sekaligus membuat hatinya bimbang.\n" +
                        "Haruskah ia mengakhiri pernikahannya dengan James atau bertahan walaupun banyak rintangan yang harus dilaluinya?"
        ));

        list.add(new Buku(
                R.drawable.rhenald_kasali,
                "Self Driving",
                "21 Februari 2018",
                "https://books.google.com/books/content/images/frontcover/EOBMDwAAQBAJ?fife=w200-h300",
                "Rhenald Kasali",
                "Sejak dilahirkan, manusia diberikan \"kendaraan\" yang kita sebut \"Self\". Hanya dengan self driving, manusia bisa mengembangkan semua potensinya dan mencapai sesuatu yang tak pernah terbayangkan. Sedangkan mentalitas passenger yang ditanam sejak kecil, dan dibiarkan para eksekutif, hanya akan menghasilkan keluhan dan keterbelengguan.\n" +
                        "\n" +
                        "Rhenald Kasali telah mengabdikan sebagian hidupnya untuk memimpin transformasi mindset. Buku ini adalah hasil kajian seorang pendidik yang pernah empat kali terlibat dalam panitia seleksi calon pimpinan KPK, calon CEO, dan pimpinan dalam jabatan publik."
        ));

        list.add(new Buku(
                R.drawable.hasanudin_abdurakhman,
                "Melawan Miskin Pikiran: Memenangkan Pertarungan Hidup ala Kang Hasan",
                "16 Oktober 2016",
                "https://books.google.com/books/content/images/frontcover/EcJmDwAAQBAJ?fife=w200-h300",
                "Hasanudin Abdurakhman",
                "“Miskin harta itu gampang sembuhnya, cukup dengan bekerja. Tetapi, kebanyakan orang itu bukan miskin harta; mereka miskin pikiran!”\n" +
                        "\n" +
                        "Demikian penulis buku ini mengutip “filosofi” sederhana dari emaknya.\n" +
                        "\n" +
                        "Ya, penyakit “miskin pikiran” inilah yang sesungguhnya hingga kini menjangkiti banyak kalangan di Indonesia dan masih belum bisa disembuhkan, apalagi dibabat habis. Miskin pikiran membuat orang terhambat meraih sukses dalam kehidupan.\n" +
                        "Buku ini mengajak kita mengubah pola-pikir (mindset) untuk lebih kreatif, lebih inovatif, dan lebih berani membuat berbagai tero-bosan baru yang radikal dalam menyiasati dan mengatasi berbagai problem yang kita hadapi, terutama dalam dunia pendidikan, dunia kerja, dan sikap beragama."
        ));

        list.add(new Buku(
                R.drawable.titi_sanaria,
                "Amore: Dongeng Tentang Waktu",
                "15 Juni 2016",
                "https://books.google.com/books/content/images/frontcover/C0tFDwAAQBAJ?fife=w200-h300",
                "Titi Sanaria",
                "Bram meninggalkanku saat upacara pernikahan kami sudah di depan mata. Begitu saja. Tanpa alasan jelas. Dan kuhabiskan ratusan hari hanya untuk berusaha melupakan laki-laki itu. Untunglah kesibukan sebagai dokter residen membantuku melangkah ke depan, apalagi kemudian aku bertemu Pram, pria yang membuatku tertarik. Aku berpikir, inilah saatnya aku melanjutkan hidup. Namun Bram tahu-tahu kembali, justru ketika aku mulai mengisi hari-hariku dengan Pram. Hatiku bimbang. Yang menyesakkan, di tengah kegalauan itu, sekali lagi aku ditinggalkan. Bukan hanya oleh Bram… atau Pram. Melainkan oleh keduanya. Sungguh, sesulit itukah mendapatkan cinta sejati? Seluruh royalti buku ini akan disumbangkan ke Rumah Baca Rebung Cendani"
        ));

        list.add(new Buku(
                R.drawable.asterlita_sv,
                "33 Cara Kaya Ala Bob Sadino: Motivasi Bisnis Anti Gagal",
                "31 Desember 2016",
                "https://books.google.com/books/content/images/frontcover/-TlbDgAAQBAJ?fife=w200-h300",
                "Asterlita SV",
                "Bob Sadino merupakan pebisnis sukses yang fenomenal. Bob Sadino memiliki kepribadian nyeleh dan perkataanya ceplas-ceplos. Meskipun ceplas-ceplos, kata-kata tersebut justru memotivasi banyak orang khususnya dalam bidang bisnis. Kata-kata Bob yang terpublikasi tersebut terekam dalam buku ini. Selain itu, buku ini juga mengupas tentang sosok Bob Sadino. Bagaimana ia memulai bisnisnya dari nol hingga menjadi pebisnis yang sangat sukses. \n" +
                        "\n" +
                        "Selain itu, buku ini memuat ajaran menjadi Goblok nan inspiratif dari sosok Bob. Pemirkiran Bob Sadino ini dibahas dari tinjauan bisnis yang sesuai. Jika Anda, ingin memulai bisnis tetapi masih memiliki banyak keraguan, maka buku ini adalah solusi yang tepat untuk masalah Anda.  Buku ini Terbitan Genesis Learning"
        ));

        list.add(new Buku(
                R.drawable.siti_nur_atika,
                "Trapped by You: Diandra Kreatif",
                "30 November 2018",
                "https://books.google.com/books/content/images/frontcover/vHKBDwAAQBAJ?fife=w200-h300",
                "Siti Nur Atika",
                "Dia menatapku sangat dalam seolah sedang mencari sesuatu yang bahkan aku tidak tahu apa itu. Hanya melalui matanya, aku tahu jika pria ini termasuk pria yang berkuasa, pria yang biasa memegang kontrol dan kendali penuh ditangannya. Setelan licin berwarna hitam miliknya membuktikan bahwa dia memiliki peranan penting di gedung perkantoran ini--- yang sejujurnya aku tidak tahu namanya siapa.\n" +
                        "\n" +
                        "Namun yang aku sadari, saat pria itu tersenyum padaku setelah kami berpandangan selama lebih dari sepuluh detik, dia akan terus mengganggu hidupku, hingga aku tak bisa lepas dari pelukannya. "
        ));

        list.add(new Buku(
                R.drawable.dian_jesika,
                "Take Me Back: Gee Publishing",
                "5 November 2018",
                "https://books.google.com/books/content/images/frontcover/6hF2DwAAQBAJ?fife=w200-h300",
                "Dian Jesika",
                "Kamu bebas dalam memilih, tetapi kamu takkan bisa terbebas dari konsekuensi pilihanmu. Nadya begitu mencintai Bram, menginginkan pria itu menjadi miliknya, hingga dia meminta Ayahnya menjodohkannya dengan pria itu. Keinginannya memang terpenuhi. Bram tidak dapat menolak perjodohan tersebut karena merasa berhutang pada Ayahnya, ia pun menikahi Nadya. Bram sudah menjadi suaminya, namun Nadya menyadari, hati pria itu tidak untuknya. Bertahun-tahun menjalani rumah tangga, sekian lama berusaha menyentuh hati Bram, akankah usaha itu membuahkan hasil? Nadya berharap begitu, karena dia sangat mencinta Bram."
        ));

        list.add(new Buku(
                R.drawable.orihim3,
                "Driving Me Crazy: Orihim3 (You&I Publisher)",
                "16 Oktober 2018",
                "https://books.google.com/books/content/images/frontcover/jMdyDwAAQBAJ?fife=w200-h300",
                "Orihim3",
                "WARNING 21++\n" +
                        " \n" +
                        "This is story menu_about Darren and Mikaela.\n" +
                        "\"Berikan dia padaku dan aku akan memberikan sekretaris terbaikku untukmu.\" Ucap Darren angkuh.\n" +
                        "\"Wow, siapa gadis ini? kau tertarik padanya?\" Leo kembali memperhatikan Foto gadis dengan mata hazel terang itu.\n" +
                        "\"Ya. Aku sangat tertarik padanya. Sampai ingin ku hancurkan hingga tak berbekas lagi.\"\n" +
                        "Ini tentang kegilaan Darren dan balas dendamnya."
        ));

        list.add(new Buku(
                R.drawable.bieliv_felixia,
                "Telepon Tengah Malam",
                "23 September 2018",
                "https://books.google.com/books/content/images/frontcover/-6lvDwAAQBAJ?fife=w200-h300",
                "Bieliv Felixia",
                "Berawal dari panggilan telepon di tengah malam di tempat kos barunya, Felicia mengalami kejadian demi kejadian yang aneh dan menyeramkan hingga membuatnya sangat ketakutan, bahkan lama kelamaan mengancam jiwanya.\n" +
                        "\n" +
                        "Sanggupkah Felicia mengatasinya seorang diri? \n" +
                        "Atau akankah ada sang penolong?\n"
        ));
    }
}
