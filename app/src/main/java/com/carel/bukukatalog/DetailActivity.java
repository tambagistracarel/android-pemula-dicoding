package com.carel.bukukatalog;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailActivity extends AppCompatActivity {

    public static String IMAGE = "IMAGE";
    public static String NAMABUKU = "NAMABUKU";
    public static String PENCIPTA = "PENCIPTA";
    public static String RELEASE = "RELEASE";
    public static String DESCRIPTION = "DESCRIPTION";

    ImageView ivImage;
    TextView namaBuku;
    TextView pencipta;
    TextView release;
    TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setupEnv();
    }

    private void setupEnv() {
        String imgPath = getIntent().getStringExtra(IMAGE);
        String nama = getIntent().getStringExtra(NAMABUKU);
        String cipta = getIntent().getStringExtra(PENCIPTA);
        String tgl = getIntent().getStringExtra(RELEASE);
        String desc = getIntent().getStringExtra(DESCRIPTION);

        ivImage = (ImageView) findViewById(R.id.iv_image);
        namaBuku = (TextView) findViewById(R.id.tv_nama_buku);
        pencipta = (TextView) findViewById(R.id.tv_pencipta);
        release = (TextView) findViewById(R.id.tv_release);
        description = (TextView) findViewById(R.id.tv_description);

        if (imgPath.isEmpty()) {
            ivImage.setVisibility(View.GONE);
        } else {
            Glide.with(this)
                    .load(imgPath)
                    .into(ivImage);
        }
        namaBuku.setText(nama);
        pencipta.setText(cipta);
        release.setText(tgl);
        description.setText(desc);
    }
}
