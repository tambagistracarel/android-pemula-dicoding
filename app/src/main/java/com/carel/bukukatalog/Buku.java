package com.carel.bukukatalog;

public class Buku {

    private int picture;
    private String judulBuku;
    private String tglRelease;
    private String gambar;
    private String pencipta;
    private String description;

    public Buku(int picture, String judulBuku, String tglRelease, String gambar, String pencipta, String description) {
        this.picture = picture;
        this.judulBuku = judulBuku;
        this.tglRelease = tglRelease;
        this.gambar = gambar;
        this.pencipta = pencipta;
        this.description = description;
    }

    public String getPencipta() {
        return pencipta;
    }

    public void setPencipta(String pencipta) {
        this.pencipta = pencipta;
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }

    public String getJudulBuku() {
        return judulBuku;
    }

    public void setJudulBuku(String judulBuku) {
        this.judulBuku= judulBuku;
    }

    public String getTglRelease() {
        return tglRelease;
    }

    public void setTglRelease(String tglRelease) {
        this.tglRelease = tglRelease;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar= gambar;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
