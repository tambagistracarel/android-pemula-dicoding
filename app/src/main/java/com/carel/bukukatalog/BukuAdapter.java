package com.carel.bukukatalog;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class BukuAdapter extends RecyclerView.Adapter<BukuAdapter.ViewHolder> {

    private List<Buku> list;

    public BukuAdapter(List<Buku> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_buku, parent, false
                )
        );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView picture;
        TextView nama;
        TextView pencipta;

        public ViewHolder(View itemView) {
            super(itemView);
            picture = (ImageView) itemView.findViewById(R.id.iv_picture);
            nama = (TextView) itemView.findViewById(R.id.txt_nama);
            pencipta = (TextView) itemView.findViewById(R.id.tv_pencipta);
        }

        public void bind(final Buku item) {
            picture.setImageResource(item.getPicture());
            nama.setText(item.getJudulBuku());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), DetailActivity.class);
                    intent.putExtra(DetailActivity.IMAGE, item.getGambar());
                    intent.putExtra(DetailActivity.NAMABUKU, item.getJudulBuku());
                    intent.putExtra(DetailActivity.PENCIPTA, item.getPencipta());
                    intent.putExtra(DetailActivity.RELEASE, item.getTglRelease());
                    intent.putExtra(DetailActivity.DESCRIPTION, item.getDescription());
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
